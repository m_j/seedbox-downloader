FTP Downloader
==========

Basic interactive script to download files from a remote ftp server
Be sure to edit the config.cfg file as well as change the hardcoded
'/downloads' folder if necessary 

License
==========

Copyright (c) Michael Johnston 2015
All code is released Under Gnu GPL v3 included in this repo under gpl.txt
