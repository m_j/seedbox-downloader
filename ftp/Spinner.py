import sys
import itertools
import threading
import time

class Spinner (threading.Thread):
    def __init__(self):
        self.message ="Running"
        pass
        threading.Thread.__init__(self)

    def setMessage (self, msg):
        self.message = msg

    def run(self):
        spinner = itertools.cycle(['-', '/', '|', '\\'])
        self.run = True
        while self.run:
            status = spinner.__next__() + " " + self.message
            sys.stdout.write(status)
            sys.stdout.flush()
            sys.stdout.write('\b'*len (status))
            time.sleep(0.1)

    def stop (self):
        self.run = False
        sys.stdout.write('\b'*20)
        sys.stdout.flush()


class Spin:
    def __init__(self):
        pass

    def spin (self,msg="Running"):
        self.spinner = Spinner()
        self.spinner.setMessage(msg)
        self.spinner.start()

    def stop (self):
        self.spinner.stop()
