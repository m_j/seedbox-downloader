#Copyright Michael Johnston 2015
import os

class Database:
    def __init__( self, database_name="downloaded.txt" ):
        self.dbname = database_name
        if (not os.path.exists(self.dbname)):
            f = open(self.dbname, 'a')
            f.close()
        self.downloaded = []

        self.read_list()

    def read_list( self ):
        dl_file = open (self.dbname, 'r')
        for download in dl_file:
            self.downloaded.append (download)
        dl_file.close()

    def is_downloaded (self, item):
        for download in self.downloaded:
            if download.replace ("\n", "") == item:
                return True
        return False

    def generate_list( self ):
            return downloaded;

    def set_downloaded (self, item):
        dl_file = open (self.dbname, 'a')
        dl_file.write (item)
        dl_file.write ("\n")
        dl_file.close()
