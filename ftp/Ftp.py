# Copyright Michael Johnston 2015

import configparser
import os
from ftplib import FTP_TLS
from sys import exit
from ftp import Spinner

class Ftp:
    def __init__(self):
        self.USE_SSL = True
        config = configparser.ConfigParser()
        config.read("/media/HDD/Downloads/ftp-downloader/config.cfg")
        # config.read ("/home/michael/projects/seedbox-downloader/config.cfg")
        #config.read("config.cfg")
        try:
            self.HOST = config["ftp"]["domain"]
            self.PORT = int(config["ftp"]["port"])
            self.USER = config["auth"]["user"]
            self.PASS = config["auth"]["pass"]
        except:
            print("Error Opening config.cfg")
            exit()

        self.ftps = None
        self.parallel_download_connections = 8

        self.connection_alive = False
        self.cached_types = {}
        s = Spinner.Spin()
        self.startSpinning = s.spin
        self.stopSpinning = s.stop


    def init_connection(self):
        if self.connection_alive: return
        self.startSpinning("Initializing Connection")
        FTP_TLS.port = self.PORT
        self.ftps = FTP_TLS(self.HOST)

        if (self.USE_SSL):
            self.ftps.auth()
            self.ftps.prot_p()

        self.ftps.login(self.USER, self.PASS)
        self.connection_alive = True
        self.stopSpinning()

    def __download_file(self, filename, local_directory, server_directory):
        filename = filename.replace("'", "\\'")
        server_directory = server_directory.replace("'", "\\'")
        local_directory = local_directory.replace("'", "\\'")

        command = "lftp -p %d %s:%s@%s" % (self.PORT, self.USER, self.PASS, self.HOST)

        if os.path.exists(os.path.join(local_directory, filename) + ".lftp-pget-status"):
            # We are continuing a download
            os.system(command + " -e \"lcd \'%s\';pget -c \'%s\';exit\""
                      % (local_directory.replace("\'", "\\'")
                         , os.path.join(server_directory, filename)
                         )
                      )
        else:
            os.system(command + " -e \"lcd \'%s\';pget -n %d \'%s\';exit\""
                      % (local_directory
                         , self.parallel_download_connections,
                         os.path.join(server_directory, filename)
                         )
                      )

        if (os.path.exists(os.path.join(local_directory, filename) + ".lftp-pget-status")):
            # Assume we havent not finised dl
            return False
        return True

    def __download_directory(self, directory, local_directory, server_directory):
        if (not os.path.exists(os.path.join(local_directory, directory))):
            os.system("mkdir \"%s\"" % (os.path.join(local_directory, directory)))

        if not self.connection_alive:
            self.init_connection()
            # self.ftp_cd ("/downloads")

        self.ftp_cd(os.path.join(server_directory, directory))

        dir_list = self.directory_list()
        self.close_connection()

        success_download = None

        for new_item in dir_list:
            if not new_item in [".", ".."]:
                result = self.download_item(
                    new_item,
                    os.path.join(local_directory, directory),
                    os.path.join(server_directory, directory)
                )

                if not result:
                    success_download = False

                elif result and success_download != False:
                    success_download = True

        self.close_connection()
        if success_download == None:
            return False

        return success_download

    # Returns type of item in ftp server,
    # either "dir" or "file"
    def getType(self, item, server_directory):
        if (server_directory, item) in self.cached_types:
            return self.cached_types[(server_directory, item)]

        self.ftp_cd(server_directory)
        self.startSpinning("Getting types for " + item)
        ls = self.ftps.mlsd(facts=["type"])
        for (name, facts) in filter(lambda x: x not in ['.', '..'], ls):
            self.cached_types[(server_directory, name)] = facts["type"]
        self.stopSpinning()
        return self.cached_types[(server_directory, item)]

    def download_item(self, item, local_directory, server_directory="/downloads"):

        self.init_connection()

        if (self.ftps.pwd() != server_directory):
            self.ftp_cd(server_directory)

        type = self.getType(item, server_directory)
        if type == "dir":
            return self.__download_directory(item, local_directory, server_directory)
        elif type == "file":
            self.close_connection()
            return self.__download_file(item, local_directory, server_directory)
        else:
            print("wtf is this type %s" % (type))

    def close_connection(self):
        self.ftps.close()
        self.connection_alive = False

    def ftp_cd(self, path):
        self.startSpinning("Cwd to "+ path)
        self.ftps.cwd(path)
        self.stopSpinning()

    def directory_list(self):
        self.startSpinning("Listing dir")
        ret = self.ftps.nlst()
        self.stopSpinning()
        return ret
