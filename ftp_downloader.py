#Code Created by Michael Johnston
#Copyright Michael Johnston 2014
#Released Under GPLv3

from ftp import Ftp
from ftp import Database
import os

class Downloader:
    def __init__ (self):
        #self.local_directory = os.path.dirname(os.path.realpath(__file__))
        self.local_directory = "/media/HDD/Downloads/"
        self.ftp = Ftp.Ftp()
        self.database = Database.Database()

        self.batch_mode = False
        self.batch_buffer = []

    def prepare_download_list( self ):
        self.ftp.init_connection()
        self.ftp.ftp_cd ("/downloads")
        dirs = self.ftp.directory_list()
        self.ftp.close_connection()

        self.download_list = self.filter_list ( dirs )
        return self.download_list

    def batch_download ( self ):
        if self.batch_mode:
            print ("Processing Batch List")
            for item in self.batch_buffer:
                print ("Preparing to Download : ", item )
                self.item_download ( item );

    def item_download ( self, item ):
        if (self.ftp.download_item( item, self.local_directory )):
            self.database.set_downloaded( item )

    def init_download ( self ):
        self.prepare_download_list()

        for item in self.download_list:
            r = self.prompt_download ( item )
            if r == "batch":
                self.batch_mode = True
                self.batch_buffer.append ( item )
            if r == "download":
                self.item_download ( item )
            if r == "no_download":
                self.database.set_downloaded( item )
            if r == "exit":
                exit ()

        if ( self.batch_mode ):
            self.batch_download ()

    def filter_list (self, download_list):
        new_download_list = []
        for item in download_list:
            if (not ( item == "." or item == ".." )
                and item.find (".meta") == -1
                and self.database.is_downloaded ( item ) == False):

                new_download_list.append (item)

        return new_download_list

    def prompt_download ( self, item):
        self.responses = ['batch', 'download','no_download','skip', 'exit']
        try:
            print (item)
        except:
            print ("Error cant output item - not ascii")
            # for debug purposes, remove later
            print ("%s" %(item[:10]))

        response = None
        while response not in self.responses:
            try:
                response = str (input("What Would you Like to do with this item? ([B]atch/[D]ownload/[N]oDownload/[S]kip/e[X]it) >> ") )
                response = response.upper()
                if response == 'B':
                    print ("Added Item to Download Batch")
                    response = "batch"
                if response == 'D':
                    print ("Downloading Item Now")
                    response = "download"
                if response == 'N':
                    print ("Will Not Download File")
                    response = "no_download"
                if response == 'S' or response == "":
                    print ("Skipping Item")
                    response = "skip"
                if response == 'X' or response == "":
                    print ("Have a nice Day :)")
                    response = "exit"
            except:
                response = None

        return response;


downloader = Downloader()
downloader.init_download()
